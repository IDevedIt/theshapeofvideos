import os

import numpy
import numpy as np
from PIL import Image


from paletters.pyimagesearch_kmean import SKLearnPaletter
from paletters.sampling_paletter import SamplingPaletter, KMeanSampler
from scene_cutter import SceneCutter
from tqdm import tqdm

if __name__ == '__main__':
    # video_prefix = "GO"
    # scene_cutter = SceneCutter("D:\Video\Séries\Good Omens\Episode1.avi")
    os.chdir("./outs")
    video_prefix = "Themis"
    scene_cutter = SceneCutter("D:\Google Drive\Cours\Post-Bac\SI4\PNS INNOV\FinalThemisAds.mp4")
    factor = 1
    frames = scene_cutter.get_scene_starting_frames()
    paletter = KMeanSampler(frames,sizing_factor=factor)
    image = paletter.get_image()
    image.show()
    image.save(f"./{video_prefix}_Scenes.png")
    try:
        paletter.save_pixels(video_prefix+"Scenes_pixels")
    finally:
        pass
    print("Starting Frames Done")
    print("Now going to the hard part")
    frames = scene_cutter.sample_frames(30)
    paletter = KMeanSampler(frames, sizing_factor=factor)
    image = paletter.get_image()
    image.show()
    try:
        paletter.save_pixels(video_prefix+"sample_pixels")
    finally:
        pass
    image.save(f"./{video_prefix}_Samples.png")